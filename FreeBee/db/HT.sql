/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.5.50-0ubuntu0.14.04.1 : Database - ht
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`freebee` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `freebee`;

/*Table structure for table `tb_follow` */

DROP TABLE IF EXISTS `tb_follow`;

CREATE TABLE `tb_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `followid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_follow` */

/*Table structure for table `tb_like` */

DROP TABLE IF EXISTS `tb_like`;

CREATE TABLE `tb_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_like` */

insert  into `tb_like`(`id`,`userid`,`product_id`) values (1,58,3),(2,6,3),(3,13,3),(4,14,3),(5,16,3),(6,2,8),(7,2,7);

/*Table structure for table `tb_product` */

DROP TABLE IF EXISTS `tb_product`;

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_price` float(10,2) NOT NULL,
  `category` int(2) NOT NULL,
  `latitude` float(15,8) NOT NULL,
  `longitude` float(15,8) NOT NULL,
  `is_sold` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_product` */

insert  into `tb_product`(`id`,`userid`,`product_name`,`product_description`,`product_price`,`category`,`latitude`,`longitude`,`is_sold`,`reg_date`) values (1,1,'keyboard','jkkkk',1.00,0,42.89496231,129.56054688,'N','2017-02-10 08:45:25'),(2,1,'keyboard','jkkkk',1.00,0,42.89516068,129.56034851,'N','2017-02-10 08:47:20'),(3,2,'iphone 6','iphone 6 koul l 16 GB',450.00,0,25.78819656,-80.21076965,'N','2017-02-14 18:01:02'),(4,6,'est','tt\nyyu',655.00,6,42.89686966,129.56401062,'N','2017-03-05 13:19:17'),(5,6,'ttttty','ttyy',666.00,5,42.89686966,129.56401062,'N','2017-03-05 13:21:16'),(6,6,'ttttty','ttyy',666.00,5,42.89749908,129.56364441,'N','2017-03-05 13:23:00'),(7,14,'Yeezy Boost 350 V2','Authentic Yeezy Boost 350 V2 with original box. Fairly new. The price is firm, no negotiation. ',550.00,0,27.48617935,-80.01721954,'N','2017-03-19 14:05:47'),(8,15,'cafe','Small hot coffee w caramel swirl',28.00,0,28.95962334,-76.54108429,'N','2017-03-19 16:12:40'),(9,2,'le Nouvelliste ','Jounal Nouvelis front page sipèsta Palman. ',25.00,0,26.15981865,-80.25295258,'N','2017-03-19 16:32:26'),(10,2,'test','iPhone 5S blan e lò 16gb',350.00,0,26.15976906,-80.25294495,'N','2017-03-22 02:15:48');

/*Table structure for table `tb_product_image` */

DROP TABLE IF EXISTS `tb_product_image`;

CREATE TABLE `tb_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_product_image` */

insert  into `tb_product_image`(`id`,`product_id`,`image`) values (1,1,'http://52.40.130.22/uploadfiles/product/2017/02/014867163257.png'),(2,2,'http://52.40.130.22/uploadfiles/product/2017/02/014867164404.png'),(3,3,'http://52.40.130.22/uploadfiles/product/2017/02/014870952623.png'),(4,4,'http://52.40.130.22/uploadfiles/product/2017/03/014887199571.png'),(5,5,'http://52.40.130.22/uploadfiles/product/2017/03/014887200763.png'),(6,6,'http://52.40.130.22/uploadfiles/product/2017/03/014887201803.png'),(7,7,'http://52.40.130.22/uploadfiles/product/2017/03/014899323479.png'),(8,8,'http://52.40.130.22/uploadfiles/product/2017/03/014899399605.png'),(9,9,'http://52.40.130.22/uploadfiles/product/2017/03/014899411463.png'),(10,10,'http://52.40.130.22/uploadfiles/product/2017/03/014901489481.png');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `userphoto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `verify_facebook` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `verify_google` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `verify_phone` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`device_id`,`username`,`email`,`password`,`phone`,`userphoto`,`verify_facebook`,`verify_google`,`verify_phone`,`reg_date`) values (1,'','beautistar1','beautistar@gmail.com','qwerty','+8615644333982','http://52.40.130.22/uploadfiles/user/2017/02/1_file14867151589.jpg','N','N','N','2017-02-10 03:36:45'),(2,'','Sergio Séide ','Sergio@htayiti.com','K9160532','9543197043','http://52.40.130.22/uploadfiles/user/2017/03/2_file14903498745.jpeg','N','N','N','2017-02-14 17:54:56'),(3,'93d7bb16a4bdf28f','','','','','','N','N','N','2017-02-17 12:46:38'),(4,'89112b7ce1a4528c','','','','','','N','N','N','2017-02-17 22:47:19'),(5,'','test05','test05@gmail.com','123456','123456789','','N','N','N','2017-03-01 02:35:25'),(6,'','test055','test055@gmail.com','123456','123456789','http://52.40.130.22/uploadfiles/user/2017/03/6_file14887198993.jpg','N','N','N','2017-03-03 12:39:55'),(7,'441a055f44a38994','','','','','','N','N','N','2017-03-05 12:46:17'),(8,'','test','test1@gmail.com','123456','123456879','','N','N','N','2017-03-05 14:19:38'),(9,'','qqq','qqq','qqq','1111111111','','N','N','N','2017-03-06 02:40:02'),(10,'f04e7615bfeafe38','','','','','','N','N','N','2017-03-08 00:56:58'),(11,'','dharma','dharmeshavaiya90@gmail.com','123456','73836 70613','http://52.40.130.22/uploadfiles/user/2017/03/11_file14896958759.jpeg','N','N','N','2017-03-16 19:20:28'),(12,'','abc','abc@gmail.com','123456','130 0909 2480','','N','N','N','2017-03-17 22:19:19'),(13,'','Roger Etienne','RJEtienne@hotmail.com','hunter74','','','N','N','N','2017-03-18 00:25:31'),(14,'','Gregory Seide','greggseide@gmail.com','100password','(954) 422-3250','http://52.40.130.22/uploadfiles/user/2017/03/14_file14898923890.jpeg','N','N','N','2017-03-19 02:55:41'),(15,'','Gigitho','guignard_v@yahoo.com','master','(954) 288-5659','','N','N','N','2017-03-19 16:03:38'),(16,'','Baudin Seide','bseide@live.com','jawoul77','(819) 329-7945','','N','N','N','2017-03-20 17:00:11');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
