/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.6.45-cll-lve : Database - freebee
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`freebee` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `freebee`;

/*Table structure for table `tb_address` */

DROP TABLE IF EXISTS `tb_address`;

CREATE TABLE `tb_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tb_address` */

insert  into `tb_address`(`id`,`userid`,`name`,`address`,`city`,`state`,`zipcode`,`created_at`) values (1,17,'Office','462 Melody Road','N Chesterfield','VA','23234','2020-02-03 12:02:05'),(2,17,'Office','462 Melody Road','N Chesterfield','VA','23234','2020-02-04 10:02:19'),(3,17,'Office','462 Melody Road','N Chesterfield','VA','23234','2020-02-04 09:02:46'),(4,18,'Office','463 Melody Road','Ns Chesterfield','VA','23230','2020-02-04 09:02:10'),(5,18,'Office12','101 Melody Road','NS Chesterfield','VA12','23230','2020-02-04 09:02:28'),(6,18,'name','address','city','state','123456','2020-02-04 10:02:16'),(7,17,'Office','462 Melody Road','N Chesterfield','VA','23234','2020-02-05 05:02:18'),(9,18,'Test','CA','Los Angels','CA','1248io','2020-02-05 10:02:25');

/*Table structure for table `tb_cart` */

DROP TABLE IF EXISTS `tb_cart`;

CREATE TABLE `tb_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tb_cart` */

/*Table structure for table `tb_follow` */

DROP TABLE IF EXISTS `tb_follow`;

CREATE TABLE `tb_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `followid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_follow` */

/*Table structure for table `tb_like` */

DROP TABLE IF EXISTS `tb_like`;

CREATE TABLE `tb_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_like` */

/*Table structure for table `tb_order` */

DROP TABLE IF EXISTS `tb_order`;

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `ordered_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tb_order` */

/*Table structure for table `tb_product` */

DROP TABLE IF EXISTS `tb_product`;

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_price` float(10,2) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float(15,8) NOT NULL,
  `longitude` float(15,8) NOT NULL,
  `quantity_available` int(11) NOT NULL,
  `quantity_per_user` int(11) NOT NULL,
  `age_min` int(3) NOT NULL,
  `age_max` int(3) NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `industry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_sold` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `sold_count` int(11) NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'regular' COMMENT 'premium,regular',
  `is_active` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes' COMMENT 'yes, no',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_product` */

insert  into `tb_product`(`id`,`userid`,`product_name`,`product_description`,`product_price`,`category`,`latitude`,`longitude`,`quantity_available`,`quantity_per_user`,`age_min`,`age_max`,`gender`,`hobby`,`industry`,`occupation`,`zipcode`,`city`,`state`,`country`,`is_sold`,`sold_count`,`type`,`is_active`,`reg_date`) values (19,19,'Coffee Mug','Nice coffee mug',0.00,'Food & Candy',25.73840332,-80.22043610,10,1,65,100,'male','marathons;boating;cricket;writing','Retail','Adjuster','33133','new york','florida','United States','N',40,'regular','yes','2019-12-11 09:58:55'),(20,19,'Stylish Cap','Stylish Cap is the best one in Summer.',0.00,'Golf, Sports & Outdoor; Caps & Hats',25.73840332,-80.22043610,10,1,65,100,'male','marathons;boating;cricket;writing','Retail','Adjuster','33133','new york','florida','United States','N',40,'regular','yes','2019-12-11 09:59:00'),(21,19,'JBL Head Phone','Very nice head phone with 5.1 effect',0.00,'Electronics, Office & Desktop',25.73840332,-80.22043610,10,1,65,100,'male','marathons;boating;cricket;writing','Retail','Adjuster','33133','new york','florida','United States','N',0,'regular','yes','2019-12-11 09:59:05'),(26,23,'OnePlus Mobile','This is last mobile phone from One Plus',0.00,'Electronics, Office & Desktop',37.43159866,78.65689850,30,5,25,34,'','','','','','N Chesterfield','VA','US','N',3,'regular','yes','2020-01-04 22:22:23'),(66,18,'Smart Cycle','Well smarted Sport cycle',0.00,'Golf, Sports & Outdoor',42.89339828,129.47853088,123,12,0,100,'','','','','','','','United State','N',0,'regular','yes','2020-01-06 03:28:47'),(77,66,'Bottles Water','Really good water',0.00,'Food & Candy',25.72945786,-80.24359131,10,2,35,44,'','','','','','','','United State','N',0,'premium','yes','2020-02-05 11:31:18'),(78,66,'Asus Laptop','Super cool laptop',0.00,'Electronics, Office & Desktop',25.73850632,-80.22033691,6,2,45,54,'male','bowling; tennis; archery; listening to music','Insurance','','','','','United State','N',0,'premium','yes','2020-02-09 09:38:06');

/*Table structure for table `tb_product_image` */

DROP TABLE IF EXISTS `tb_product_image`;

CREATE TABLE `tb_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_product_image` */

insert  into `tb_product_image`(`id`,`product_id`,`image`) values (27,19,'http://free-bee.co/uploadfiles/product/2019/12/015760835352.png'),(28,20,'http://free-bee.co/uploadfiles/product/2019/12/015760835402.png'),(29,21,'http://free-bee.co/uploadfiles/product/2019/12/015760835457.png'),(34,25,'http://free-bee.co/uploadfiles/product/2020/01/015781422867.png'),(35,26,'http://free-bee.co/uploadfiles/product/2020/01/015782017431.png'),(83,62,'http://free-bee.co/uploadfiles/product/2020/01/015782885615.png'),(88,66,'http://free-bee.co/uploadfiles/product/2020/01/015783065278.png'),(94,70,'http://free-bee.co/uploadfiles/product/2020/01/015785459861.png'),(96,73,'http://free-bee.co/uploadfiles/product/2020/01/015802216368.png'),(97,74,'http://free-bee.co/uploadfiles/product/2020/01/015802255699.png'),(98,74,'http://free-bee.co/uploadfiles/product/2020/01/115802255699.png'),(99,75,'http://free-bee.co/uploadfiles/product/2020/01/015802268998.png'),(100,75,'http://free-bee.co/uploadfiles/product/2020/01/115802268998.png'),(102,77,'http://free-bee.co/uploadfiles/product/2020/02/015809274784.png'),(103,78,'http://free-bee.co/uploadfiles/product/2020/02/015812662871.png'),(107,82,'http://free-bee.co/uploadfiles/product/2020/02/015812704097.png');

/*Table structure for table `tb_transaction` */

DROP TABLE IF EXISTS `tb_transaction`;

CREATE TABLE `tb_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tb_transaction` */

insert  into `tb_transaction`(`id`,`userid`,`transaction_id`,`amount`,`created_at`) values (1,18,'ch_332423423442',5.00,'2020-02-18 03:02:47'),(2,19,'ch_12433523242',20.00,'2020-02-18 03:02:58'),(3,19,'ch_12433523242',-5.00,'2020-02-18 03:02:55'),(4,18,'ch_332423423442',5.00,'2020-02-18 03:02:52'),(5,18,'ch_332423423442',5.00,'2020-02-18 03:02:21'),(6,18,'ch_332423423442',5.00,'2020-02-18 03:02:20');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `userphoto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `verify_facebook` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `verify_google` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `verify_phone` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(3) NOT NULL,
  `balance` float(10,2) NOT NULL,
  `token` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`device_id`,`username`,`email`,`password`,`phone`,`userphoto`,`verify_facebook`,`verify_google`,`verify_phone`,`address`,`city`,`state`,`zip_code`,`gender`,`age`,`balance`,`token`,`reg_date`) values (1,'','beautistar1','beautistar@gmail.com','qwerty','+8615644333982','http://52.40.130.22/uploadfiles/user/2017/02/1_file14867151589.jpg','N','N','N','','','','','',0,0.00,'sdfasdfasdfsdRErdfdsgda2343tgfge4t4','2017-02-10 03:36:45'),(3,'93d7bb16a4bdf28f','','','','','','N','N','N','','','','','',0,0.00,'','2017-02-17 12:46:38'),(4,'89112b7ce1a4528c','','','','','','N','N','N','','','','','',0,0.00,'','2017-02-17 22:47:19'),(5,'','test05','test05@gmail.com','123456','123456789','','N','N','N','','','','','',0,0.00,'','2017-03-01 02:35:25'),(6,'','test055','test055@gmail.com','123456','123456789','http://52.40.130.22/uploadfiles/user/2017/03/6_file14887198993.jpg','N','N','N','','','','','',0,0.00,'','2017-03-03 12:39:55'),(7,'441a055f44a38994','','','','','','N','N','N','','','','','',0,0.00,'','2017-03-05 12:46:17'),(8,'','test','test1@gmail.com','123456','123456879','','N','N','N','','','','','',0,0.00,'','2017-03-05 14:19:38'),(9,'','qqq','qqq','qqq','1111111111','','N','N','N','','','','','',0,0.00,'','2017-03-06 02:40:02'),(10,'f04e7615bfeafe38','','','','','','N','N','N','','','','','',0,0.00,'','2017-03-08 00:56:58'),(12,'','abc','abc@gmail.com','123456','130 0909 2480','','N','N','N','','','','','',0,0.00,'','2017-03-17 22:19:19'),(13,'','Roger Etienne','RJEtienne@hotmail.com','hunter74','','','N','N','N','','','','','',0,0.00,'','2017-03-18 00:25:31'),(14,'','Gregory Seide','greggseide@gmail.com','100password','(954) 422-3250','http://52.40.130.22/uploadfiles/user/2017/03/14_file14898923890.jpeg','N','N','N','','','','','',0,0.00,'','2017-03-19 02:55:41'),(15,'','Gigitho','guignard_v@yahoo.com','master','(954) 288-5659','','N','N','N','','','','','',0,0.00,'','2017-03-19 16:03:38'),(16,'','Baudin Seide','bseide@live.com','jawoul77','(819) 329-7945','','N','N','N','','','','','',0,0.00,'','2017-03-20 17:00:11'),(17,'','we','rapanjel@hotmail.com','12345','8616742680599','http://free-bee.co/uploadfiles/user/2019/12/17_file15755282410.jpeg','N','Y','N','','','','','',0,0.00,'','2019-11-30 19:53:35'),(18,'','Tester0502','test0502@gmail.com','123456','1234567890','http://free-bee.co/uploadfiles/user/2020/02/18_file15819419418.jpeg','N','N','N','','','','','',0,0.00,'','2019-12-01 00:18:26'),(19,'','Ian Bandklayder','ian.bandklayder@gmail.com','sharpoi7','7895129368','','N','N','N','','','','','',0,0.00,'','2019-12-01 19:23:45'),(20,'','tester','test@user.com','123456','1234568','','N','N','N','','','','','',0,0.00,'','2019-12-03 00:54:52'),(21,'','tttt','rr@gmail.com','qwer','12345678','','N','N','N','','','','','',0,0.00,'','2019-12-03 00:57:18'),(22,'','Testera','test11@user.com','123456','1236780','','N','N','N','','','','','',0,0.00,'','2019-12-03 01:22:24'),(23,'','testabc','test100@user.com','123456','112233','','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,0.00,'','2019-12-03 04:17:01'),(24,'','test_postman','test@postman.com','123456','112233','','N','N','N','','','','','',0,0.00,'','2019-12-03 20:11:01'),(25,'','test_postman1','test1@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215754291542.jfif','N','N','N','','','','','',0,0.00,'','2019-12-03 20:12:34'),(26,'','test_postman1','test1@postman.com','123456','112233','','N','N','N','','','','','',0,0.00,'','2019-12-03 20:12:34'),(27,'','Test0502','test0502@gmail.com','123456','123456000','','N','N','N','','','','','',0,0.00,'','2019-12-04 06:12:00'),(28,'','Test52','test52@gmail.com','123456','86555666333','','N','N','N','','','','','',0,0.00,'','2019-12-04 06:16:59'),(29,'','Test520','test520@gmail.com','123456','865955234','http://free-bee.co/uploadfiles/2019/12/20191215754658689.png','N','N','N','','','','','',0,0.00,'','2019-12-04 06:24:28'),(34,'','test','test@facebook.com','111222333444556','','','Y','N','N','','','','','',0,0.00,'','2019-12-11 08:12:39'),(49,'','test_postman122','test11111@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760706874.jpg','N','N','N','','','','','',0,0.00,'','2019-12-11 06:24:47'),(51,'','test_postman1111','test111@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760707045.png','N','N','N','','','','','',0,0.00,'','2019-12-11 06:25:04'),(52,'','test_postman1111','test111@postman.com','123456','112233','','N','N','N','','','','','',0,0.00,'','2019-12-11 06:25:04'),(53,'','a','a@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760709087.jpg','N','N','N','','','','','',0,0.00,'','2019-12-11 06:28:28'),(54,'','aa','aa@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760709806.jpg','N','N','N','','','','','',0,0.00,'','2019-12-11 06:29:40'),(55,'','test_postman1112','test112@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760711423.png','N','N','N','','','','','',0,0.00,'','2019-12-11 06:32:22'),(56,'','Guimin Zhang','zhangguimin0604@gmail.com','593169434779039','123456','http://graph.facebook.com/593169434779039/picture?type=large','Y','N','N','aaa','bbb','fa','qwe','male',30,0.00,'','2019-12-11 03:12:24'),(57,'','Guimin Zhang','zhangguimin0604@gmail.com','593169434779039','','http://graph.facebook.com/593169434779039/picture?type=large','Y','N','N','','','','','',0,0.00,'','2019-12-11 03:12:24'),(58,'','Gold Rain','goldrain0502@gmail.com','104783893878652747961','056544','','N','Y','N','ddd','dgggg','ffgg','xcvvv','Male',30,0.00,'','2019-12-11 03:12:43'),(59,'','Gold Rain','goldrain0502@gmail.com','104783893878652747961','','','N','Y','N','','','','','',0,0.00,'','2019-12-11 03:12:43'),(60,'','aaa','aaa@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215764982758.jpg','Y','Y','N','richmond 12','N Chesterfield','VA','23234','male',33,0.00,'','2019-12-16 05:11:15'),(61,'','Ling Li','saville1986713@gmail.com','161304218562291','','http://graph.facebook.com/161304218562291/picture?type=large','Y','N','N','','','','','',0,0.00,'','2019-12-28 01:12:31'),(62,'','Ling Li','saville1986713@gmail.com','161304218562291','','http://graph.facebook.com/161304218562291/picture?type=large','Y','N','N','','','','','',0,0.00,'','2019-12-28 01:12:31'),(65,'','tyggg','rms272@hotmail.com','12345','123456489','http://free-bee.co/uploadfiles/2019/12/20191215775059020.jpeg','N','N','N','we’re','Fort Lauderdale KFXE (Executive);Fort Myers KFMY','Florida;West Virginia','55555','male',23,0.00,'','2019-12-27 21:05:02'),(66,'','Ian','Ian.bandklayder@gmail.co','sharpo87','7865129368','http://free-bee.co/uploadfiles/2019/12/20191215775470924.jpeg','N','N','N','11500 ','Fort Lauderdale KFLL (International)','Florida','33186','male',32,0.00,'','2019-12-28 08:31:32'),(67,'','www','www@hotmail.com','12345','1234567890','http://free-bee.co/uploadfiles/2019/12/20191215776739549.jpeg','N','N','N','ttttt','Lawrence KLWM;Montague 0B5;Nantucket KACK;New Bedf','Massachusetts;North Dakota','12345','male',12,0.00,'','2019-12-29 19:45:54'),(68,'','aaa11','aaa11@postman.com','123456','112233','','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,0.00,'','2020-01-04 07:12:04'),(69,'','aaa111','aaa111@postman.com','123456','112233','http://free-bee.co/uploadfiles/2020/01/20200115781471472.png','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,0.00,'','2020-01-04 07:12:27'),(70,'','Ian band','ian@gmail.com','sharpo87','7865129368','http://free-bee.co/uploadfiles/2020/01/20200115792276583.jpeg','N','N','N','114550','Miami KTMB (Executive)','Florida','33133','male',32,0.00,'','2020-01-16 19:20:58'),(72,'','Ian Bandklayder','ian.bandklayder@gmail.com','10115042547645783','7865129368','http://graph.facebook.com/10115042547645783/picture?type=large','Y','N','N','1120','Durant KDUA','Oklahoma','33133','male',32,0.00,'','2020-01-28 01:01:39'),(73,'','Daniel','ian@gmail.biz','sharpo87','7865129368','http://free-bee.co/uploadfiles/2020/02/20200215806091416.jpeg','N','N','N','1134','Mountain Home U76','Idaho','33155','male',25,0.00,'','2020-02-01 19:05:41'),(74,'','tr','test001@gmail.com','123456','1234567890','','N','N','N','123456','ADELL;ALBANY','Wisconsin;South Dakota','123','male',32,0.00,'','2020-02-05 13:17:11'),(75,'','aaa1','aaa1@postman.com','123456','1122331','','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,0.00,'','2020-02-05 14:53:09'),(76,'','aaa2','aaa3@postman.com','123456','11223311','http://free-bee.co/uploadfiles/2020/02/20200215809523246.png','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,0.00,'','2020-02-05 18:25:24'),(77,'','My Angel','rapanjel727@gmail.com','109725293526507944983','','https://lh3.googleusercontent.com/a-/AAuE7mC-l54lUVAPKVQg6qvjS4fQFKayXuRvpIldqy7I=s300','N','Y','N','','','','','',0,0.00,'','2020-02-09 06:02:33'),(78,'','My Angel','rapanjel727@gmail.com','109725293526507944983','','https://lh3.googleusercontent.com/a-/AAuE7mC-l54lUVAPKVQg6qvjS4fQFKayXuRvpIldqy7I=s300','N','Y','N','','','','','',0,0.00,'','2020-02-09 06:02:33'),(81,'','Dana Gore','danahere2@yahoo.com','Makenna2','8122057422','http://free-bee.co/uploadfiles/2020/02/20200215817080196.jpeg','N','N','N','6037 poinsettia cove','EVANSVILLE','Indiana','47715','female',49,0.00,'','2020-02-14 12:20:19'),(82,'','Chrislea Cosingan','cchrislea@gmail.com','102042275833352509428','','https://lh3.googleusercontent.com/a-/AAuE7mAacvbTHG9jlYAJ_hOt22NBs2DBFqwZVydKOWUjSQ=s300','N','Y','N','','','','','',0,0.00,'','2020-02-16 02:02:30'),(83,'','Chrislea Cosingan','cchrislea@gmail.com','102042275833352509428','','https://lh3.googleusercontent.com/a-/AAuE7mAacvbTHG9jlYAJ_hOt22NBs2DBFqwZVydKOWUjSQ=s300','N','Y','N','','','','','',0,0.00,'','2020-02-16 02:02:30'),(84,'','Martin Wessels','martinwessels@hotmail.com','2669268963193298','','http://graph.facebook.com/2669268963193298/picture?type=large','Y','N','N','','','','','',0,0.00,'','2020-02-17 02:02:16'),(85,'','Martin Wessels','martinwessels@hotmail.com','2669268963193298','','http://graph.facebook.com/2669268963193298/picture?type=large','Y','N','N','','','','','',0,0.00,'','2020-02-17 02:02:16');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
