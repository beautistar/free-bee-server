/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.6.45-cll-lve : Database - freebee
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`freebee` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `freebee`;

/*Table structure for table `tb_cart` */

DROP TABLE IF EXISTS `tb_cart`;

CREATE TABLE `tb_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `tb_cart` */

insert  into `tb_cart`(`id`,`userid`,`productid`,`count`) values (45,17,18,9),(22,19,18,0),(25,19,21,0),(24,19,15,0),(43,18,21,1);

/*Table structure for table `tb_follow` */

DROP TABLE IF EXISTS `tb_follow`;

CREATE TABLE `tb_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `followid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_follow` */

/*Table structure for table `tb_like` */

DROP TABLE IF EXISTS `tb_like`;

CREATE TABLE `tb_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_like` */

insert  into `tb_like`(`id`,`userid`,`product_id`) values (1,58,3),(2,6,3),(3,13,3),(4,14,3),(5,16,3),(6,2,8),(7,2,7),(13,17,17),(19,17,16),(20,17,10),(22,18,10),(23,18,17),(24,18,3),(25,18,19),(26,18,20),(27,17,18),(28,17,24),(29,18,0);

/*Table structure for table `tb_product` */

DROP TABLE IF EXISTS `tb_product`;

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_price` float(10,2) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float(15,8) NOT NULL,
  `longitude` float(15,8) NOT NULL,
  `quantity_available` int(11) NOT NULL,
  `quantity_per_user` int(11) NOT NULL,
  `age_min` int(3) NOT NULL,
  `age_max` int(3) NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `industry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_sold` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `sold_count` int(11) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_product` */

insert  into `tb_product`(`id`,`userid`,`product_name`,`product_description`,`product_price`,`category`,`latitude`,`longitude`,`quantity_available`,`quantity_per_user`,`age_min`,`age_max`,`gender`,`hobby`,`industry`,`occupation`,`zipcode`,`city`,`state`,`country`,`is_sold`,`sold_count`,`reg_date`) values (1,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(2,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(3,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(4,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(5,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(6,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(7,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(8,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(9,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(10,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(11,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(12,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(13,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(14,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(15,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(16,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(17,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(18,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',9,'2019-12-26 21:30:28'),(19,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(20,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(21,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(22,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','sdfsdfsdfs','ghi','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:30:28'),(23,0,'test','test',15.00,'0',37.43159866,78.65689850,30,5,25,34,'male','abc','AAAAAAAAAAAAAAAAAAAAA','ghiAAAAAAAAAAAAA','23234','N Chesterfield','VA','US','N',0,'2019-12-26 21:32:52'),(24,17,'rrrrrr','Cvvghhcv ggg hhh fhj ',0.00,'',42.89345551,129.47856140,121,12,0,0,'','','','','','','','','N',0,'2019-12-26 23:00:08'),(25,17,'ertrf','Dffffftfggg vvv v vvv v v. Vvvv',0.00,'',42.89345551,129.47856140,5555,124,0,0,'','','','','','','','','N',0,'2019-12-26 23:02:59');

/*Table structure for table `tb_product_image` */

DROP TABLE IF EXISTS `tb_product_image`;

CREATE TABLE `tb_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_product_image` */

insert  into `tb_product_image`(`id`,`product_id`,`image`) values (1,1,'http://52.40.130.22/uploadfiles/product/2017/02/014867163257.png'),(2,2,'http://52.40.130.22/uploadfiles/product/2017/02/014867164404.png'),(3,3,'http://52.40.130.22/uploadfiles/product/2017/02/014870952623.png'),(4,4,'http://52.40.130.22/uploadfiles/product/2017/03/014887199571.png'),(5,5,'http://52.40.130.22/uploadfiles/product/2017/03/014887200763.png'),(6,6,'http://52.40.130.22/uploadfiles/product/2017/03/014887201803.png'),(7,7,'http://52.40.130.22/uploadfiles/product/2017/03/014899323479.png'),(8,8,'http://52.40.130.22/uploadfiles/product/2017/03/014899399605.png'),(9,9,'http://52.40.130.22/uploadfiles/product/2017/03/014899411463.png'),(10,10,'http://52.40.130.22/uploadfiles/product/2017/03/014901489481.png'),(11,11,'http://52.40.130.22/uploadfiles/product/2019/12/015751697604.png'),(12,12,'http://52.40.130.22/uploadfiles/product/2019/12/015751698538.png'),(13,13,'http://52.40.130.22/uploadfiles/product/2019/12/015754266460.png'),(14,13,'http://52.40.130.22/uploadfiles/product/2019/12/115754266460.png'),(15,14,'http://52.40.130.22/uploadfiles/product/2019/12/015754267485.png'),(16,14,'http://52.40.130.22/uploadfiles/product/2019/12/115754267485.png'),(17,15,'http://free-bee.co/uploadfiles/product/2019/12/015754269761.png'),(18,15,'http://free-bee.co/uploadfiles/product/2019/12/115754269761.png'),(19,16,'http://free-bee.co/uploadfiles/product/2019/12/015754280609.png'),(20,16,'http://free-bee.co/uploadfiles/product/2019/12/115754280609.png'),(21,17,'http://free-bee.co/uploadfiles/product/2019/12/015755065046.png'),(22,17,'http://free-bee.co/uploadfiles/product/2019/12/115755065046.png'),(23,17,'http://free-bee.co/uploadfiles/product/2019/12/215755065046.png'),(24,18,'http://free-bee.co/uploadfiles/product/2019/12/015755623200.png'),(25,18,'http://free-bee.co/uploadfiles/product/2019/12/115755623200.png'),(26,18,'http://free-bee.co/uploadfiles/product/2019/12/215755623201.png'),(27,19,'http://free-bee.co/uploadfiles/product/2019/12/015760835352.png'),(28,20,'http://free-bee.co/uploadfiles/product/2019/12/015760835402.png'),(29,21,'http://free-bee.co/uploadfiles/product/2019/12/015760835457.png'),(30,22,'http://free-bee.co/uploadfiles/product/2019/12/015774194391.png'),(31,23,'http://free-bee.co/uploadfiles/product/2019/12/015774195872.png'),(32,23,'http://free-bee.co/uploadfiles/product/2019/12/115774195872.png'),(33,24,'http://free-bee.co/uploadfiles/product/2019/12/015774264089.png'),(34,25,'http://free-bee.co/uploadfiles/product/2019/12/015774265795.png');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `userphoto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `verify_facebook` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `verify_google` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `verify_phone` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(3) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`device_id`,`username`,`email`,`password`,`phone`,`userphoto`,`verify_facebook`,`verify_google`,`verify_phone`,`address`,`city`,`state`,`zip_code`,`gender`,`age`,`reg_date`) values (1,'','beautistar1','beautistar@gmail.com','qwerty','+8615644333982','http://52.40.130.22/uploadfiles/user/2017/02/1_file14867151589.jpg','N','N','N','','','','','',0,'2017-02-10 03:36:45'),(2,'','Sergio Séide ','Sergio@htayiti.com','K9160532','9543197043','http://52.40.130.22/uploadfiles/user/2017/03/2_file14903498745.jpeg','N','N','N','','','','','',0,'2017-02-14 17:54:56'),(3,'93d7bb16a4bdf28f','','','','','','N','N','N','','','','','',0,'2017-02-17 12:46:38'),(4,'89112b7ce1a4528c','','','','','','N','N','N','','','','','',0,'2017-02-17 22:47:19'),(5,'','test05','test05@gmail.com','123456','123456789','','N','N','N','','','','','',0,'2017-03-01 02:35:25'),(6,'','test055','test055@gmail.com','123456','123456789','http://52.40.130.22/uploadfiles/user/2017/03/6_file14887198993.jpg','N','N','N','','','','','',0,'2017-03-03 12:39:55'),(7,'441a055f44a38994','','','','','','N','N','N','','','','','',0,'2017-03-05 12:46:17'),(8,'','test','test1@gmail.com','123456','123456879','','N','N','N','','','','','',0,'2017-03-05 14:19:38'),(9,'','qqq','qqq','qqq','1111111111','','N','N','N','','','','','',0,'2017-03-06 02:40:02'),(10,'f04e7615bfeafe38','','','','','','N','N','N','','','','','',0,'2017-03-08 00:56:58'),(11,'','dharma','dharmeshavaiya90@gmail.com','123456','73836 70613','http://52.40.130.22/uploadfiles/user/2017/03/11_file14896958759.jpeg','N','N','N','','','','','',0,'2017-03-16 19:20:28'),(12,'','abc','abc@gmail.com','123456','130 0909 2480','','N','N','N','','','','','',0,'2017-03-17 22:19:19'),(13,'','Roger Etienne','RJEtienne@hotmail.com','hunter74','','','N','N','N','','','','','',0,'2017-03-18 00:25:31'),(14,'','Gregory Seide','greggseide@gmail.com','100password','(954) 422-3250','http://52.40.130.22/uploadfiles/user/2017/03/14_file14898923890.jpeg','N','N','N','','','','','',0,'2017-03-19 02:55:41'),(15,'','Gigitho','guignard_v@yahoo.com','master','(954) 288-5659','','N','N','N','','','','','',0,'2017-03-19 16:03:38'),(16,'','Baudin Seide','bseide@live.com','jawoul77','(819) 329-7945','','N','N','N','','','','','',0,'2017-03-20 17:00:11'),(17,'','we','rapanjel@hotmail.com','12345','8616742680599','http://free-bee.co/uploadfiles/user/2019/12/17_file15755282410.jpeg','N','Y','N','','','','','',0,'2019-11-30 19:53:35'),(18,'','Tester0502','test0502@gmail.com','123456','1234567890','','N','N','N','','','','','',0,'2019-12-01 00:18:26'),(19,'','Ian Bandklayder','ian.bandklayder@gmail.com','sharpoi7','7895129368','','N','N','N','','','','','',0,'2019-12-01 19:23:45'),(20,'','tester','test@user.com','123456','1234568','','N','N','N','','','','','',0,'2019-12-03 00:54:52'),(21,'','tttt','rr@gmail.com','qwer','12345678','','N','N','N','','','','','',0,'2019-12-03 00:57:18'),(22,'','Testera','test11@user.com','123456','1236780','','N','N','N','','','','','',0,'2019-12-03 01:22:24'),(23,'','testabc','test100@user.com','123456','112233','','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,'2019-12-03 04:17:01'),(24,'','test_postman','test@postman.com','123456','112233','','N','N','N','','','','','',0,'2019-12-03 20:11:01'),(25,'','test_postman1','test1@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215754291542.jfif','N','N','N','','','','','',0,'2019-12-03 20:12:34'),(26,'','test_postman1','test1@postman.com','123456','112233','','N','N','N','','','','','',0,'2019-12-03 20:12:34'),(27,'','Test0502','test0502@gmail.com','123456','123456000','','N','N','N','','','','','',0,'2019-12-04 06:12:00'),(28,'','Test52','test52@gmail.com','123456','86555666333','','N','N','N','','','','','',0,'2019-12-04 06:16:59'),(29,'','Test520','test520@gmail.com','123456','865955234','http://free-bee.co/uploadfiles/2019/12/20191215754658689.png','N','N','N','','','','','',0,'2019-12-04 06:24:28'),(34,'','test','test@facebook.com','111222333444556','','','Y','N','N','','','','','',0,'2019-12-11 08:12:39'),(49,'','test_postman122','test11111@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760706874.jpg','N','N','N','','','','','',0,'2019-12-11 06:24:47'),(51,'','test_postman1111','test111@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760707045.png','N','N','N','','','','','',0,'2019-12-11 06:25:04'),(52,'','test_postman1111','test111@postman.com','123456','112233','','N','N','N','','','','','',0,'2019-12-11 06:25:04'),(53,'','a','a@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760709087.jpg','N','N','N','','','','','',0,'2019-12-11 06:28:28'),(54,'','aa','aa@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760709806.jpg','N','N','N','','','','','',0,'2019-12-11 06:29:40'),(55,'','test_postman1112','test112@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215760711423.png','N','N','N','','','','','',0,'2019-12-11 06:32:22'),(56,'','Guimin Zhang','zhangguimin0604@gmail.com','593169434779039','','http://graph.facebook.com/593169434779039/picture?type=large','Y','N','N','','','','','',0,'2019-12-11 03:12:24'),(57,'','Guimin Zhang','zhangguimin0604@gmail.com','593169434779039','','http://graph.facebook.com/593169434779039/picture?type=large','Y','N','N','','','','','',0,'2019-12-11 03:12:24'),(58,'','Gold Rain','goldrain0502@gmail.com','104783893878652747961','','','N','Y','N','','','','','',0,'2019-12-11 03:12:43'),(59,'','Gold Rain','goldrain0502@gmail.com','104783893878652747961','','','N','Y','N','','','','','',0,'2019-12-11 03:12:43'),(60,'','aaa','aaa@postman.com','123456','112233','http://free-bee.co/uploadfiles/2019/12/20191215764982758.jpg','N','N','N','richmond 12','N Chesterfield','VA','23234','male',33,'2019-12-16 05:11:15'),(61,'','android tester124','androidtester123@gmail.com','123456','1234567890','http://free-bee.co/uploadfiles/2019/12/20191215765091262.png','N','N','N','tester','tester','tester','tester','male',30,'2019-12-16 08:12:06');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
