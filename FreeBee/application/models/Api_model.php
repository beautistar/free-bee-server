<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }     
    
    function existUserName($username) {

        $this->db->where('username', $username);         
        return $this->db->get('tb_user')->num_rows();
    }
    
    function existEmail($email) {

        $this->db->where('email', $email);         
        return $this->db->get('tb_user')->num_rows();
    }   
    
    function register($username, $email, $password, $phone, $address, $city, $state, $zip_code, $age, $gender) {
        
        $this->db->set('email', $email);
        $this->db->set('username', $username);
        $this->db->set('password', $password);
        $this->db->set('phone', $phone);
        $this->db->set('address', $address);
        $this->db->set('city', $city);
        $this->db->set('state', $state);
        $this->db->set('zip_code', $zip_code);
        $this->db->set('age', $age);
        $this->db->set('gender', $gender);
        $this->db->set('reg_date', 'NOW()',false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function update_photo($id, $data) {
        
        $this->db->where('id', $id);
        $this->db->update('tb_user', $data);
    }
    
    function saveProduct($p_name, $p_description, $p_price, $category, $lat, $lon, $userid, $type, 
    $quantity_available, $quantity_per_user, $age_min, $age_max, $gender, $hobby, $industry, $occupation, 
    $zipcode, $city, $state, $country) {
        
        $this->db->set('product_name', $p_name);
        $this->db->set('product_description', $p_description);
        $this->db->set('product_price', $p_price);
        $this->db->set('category', $category);
        $this->db->set('latitude', $lat);
        $this->db->set('longitude', $lon);
        $this->db->set('userid', $userid);
        $this->db->set('type', $type);
        $this->db->set('quantity_available', $quantity_available);
        $this->db->set('quantity_per_user', $quantity_per_user);
        $this->db->set('age_min', $age_min);
        $this->db->set('age_max', $age_max);
        $this->db->set('gender', $gender);
        $this->db->set('hobby', $hobby);
        $this->db->set('industry', $industry);
        $this->db->set('occupation', $occupation);
        $this->db->set('zipcode', $zipcode);
        $this->db->set('state', $state);
        $this->db->set('city', $city);
        $this->db->set('country', $country);
        $this->db->set('reg_date', 'NOW()', false);
        
        $this->db->insert('tb_product');
        return $this->db->insert_id();
        
    }
    
    function saveProductFile($product_id, $file_url) {
        
        $this->db->set('product_id', $product_id);
        $this->db->set('image', $file_url); 
        
        $this->db->insert('tb_product_image');
        $this->db->insert_id();        
    }
    
    function product_list($page, $age, $gender) {
        
        $result = array(); 
        $start =  ($page - 1) * 10; 
        if ($age != 0) {
            $this->db->where('age_min <', $age);       
            $this->db->where('age_max >', $age);            
        }
        if ($gender != "") {
            if ($gender == "male") {
                $this->db->where('gender !=', 'female'); 
            } else {
                $this->db->where('gender !=', "male");
            }      
        }   
        
        $this->db->where('quantity_available > ', 0);       
        $this->db->where('is_active', 'yes');       
        $this->db->limit(10, $start);
        $this->db->order_by('reg_date', 'DESC');
        $query = $this->db->get('tb_product');
        
        if ($query->num_rows() == 0) return 0;
        
        else {    
            foreach($query->result() as $product) {
                
                $query_image = $this->db->where('product_id', $product->id)->get('tb_product_image');
                if ($query_image->num_rows() > 0) {
                    $image = $query_image->row()->image;
                } else {
                    $image = "N/A";
                }
                $arr = array('product_id' => $product->id,
                             'product_name' => $product->product_name,
                             'product_image' => $image);
                array_push($result, $arr);
            }
            
            return $result;        
        }
    }
    
    function product_detail($user_id, $product_id) {
        
        $result = array();
        
        $this->db->order_by('reg_date', 'DESC');
        $query = $this->db->where('id', $product_id)->get('tb_product');
        
        if($query->num_rows() > 0) {
            
            $product = $query->row();
                
            // file urls
            $file_urls = array();
            $this->db->where('product_id', $product->id);
            $p_query = $this->db->get('tb_product_image');
            $i = 0;
            foreach($p_query->result() as $image) {
                
                $file_urls[$i] = $image->image;
                $i++;
            }
            
            //is like
            $is_liked = 'U';
            
            $this->db->where('userid', $user_id);
            $this->db->where('product_id', $product_id);
            if ($this->db->get('tb_like')->num_rows() > 0) {
                $is_liked = 'L';
            }
            
            // is added to cart
            $cart_added = "N";
            $this->db->where('userid', $user_id);
            $this->db->where('productid', $product_id);
            if ($this->db->get('tb_cart')->num_rows() > 0) {
                $cart_added = 'Y';
            }
            
            //owner
            $user = $this->db->where('id', $product->userid)->get('tb_user')->row();
                    
            // count mutual friend
            $friend = 1;
                                    
            $result['product_id'] = $product->id;
            $result['product_name'] = $product->product_name;
            $result['product_description'] = $product->product_description;
            $result['product_price'] = $product->product_price;
            $result['category'] = $product->category;
            $result['product_latitude'] = $product->latitude;
            $result['product_longitude'] = $product->longitude;
            
            $result['quantity_available'] = $product->quantity_available;
            $result['quantity_per_user'] = $product->quantity_per_user;
            $result['age_min'] = $product->age_min;
            $result['age_max'] = $product->age_max;
            $result['gender'] = $product->gender;
            $result['hobby'] = $product->hobby;
            $result['industry'] = $product->industry;
            $result['occupation'] = $product->occupation;
            $result['zipcode'] = $product->zipcode;
            $result['city'] = $product->city;
            $result['state'] = $product->state;
            $result['country'] = $product->country;              
            $result['type'] = $product->type;              
            
            $result['is_liked'] = $is_liked;
            $result['cart_added'] = $cart_added;
            $result['product_image'] = $file_urls;
            
            $result['product_owner_id'] = $user->id;
            $result['product_owner_name'] = $user->username;
            $result['product_owner_image'] = $user->userphoto;
            $result['product_owner_phone'] = $user->phone;
            $result['verify_facebook'] = $user->verify_facebook;  
            $result['verify_google'] = $user->verify_google; 
            $result['count_mutual_friend'] = $friend;
            $result['is_sold'] = $product->is_sold;
            $result['sold_count'] = $product->sold_count;
            
            return $result;    
        } else {
            
            return 0;
        }
    }
    
    function search_product($price_from, $price_to, $categories) {
        
        $result = array();
        $categories = explode(',', $categories);
        $this->db->order_by('reg_date', 'DESC');
        if ($price_from != 0) {
            $this->db->where('product_price >= ', $price_from);
        }
        if ($price_to != 9999) {
            $this->db->where('product_price <= ', $price_to);
        }
        $this->db->where('is_active', 'yes');
        $this->db->where('quantity_available > ', 0);
        $this->db->where_in('category', $categories);
        $query = $this->db->get('tb_product');
        
        if ($query->num_rows() == 0) return 0;
        
        else {    
            foreach($query->result() as $product) {
                
                $image = $this->db->where('product_id', $product->id)->get('tb_product_image')->row()->image;
                
                $arr = array('product_id' => $product->id,
                             'product_name' => $product->product_name,
                             'product_image' => $image);
                array_push($result, $arr);
            }
            
            return $result;        
        }
    }
    
    function userFollowing($userid) {
        
        $result = array();
        
        $this->db->where('userid', $userid);
        $query = $this->db->get('tb_follow');
        
        if($query->num_rows() == 0) return 0;
        
        else {
            foreach($query->result() as $follow) {
                
                $user = $this->db->where('id', $follow->followid)->get('tb_user')->row();
                
                $arr = array('userid' => $user->id,
                             'username' => $user->username,
                             'phone' => $user->phone,
                             'userimage' => $user->userphoto);
                array_push($result, $arr);
            }
            
            return $result;
        }
    }
    
    function userFollower($userid) {
        
        $result = array();
        
        $this->db->where('followid', $userid);
        $query = $this->db->get('tb_follow');
        
        if($query->num_rows() == 0) return 0;
        
        else {
            foreach($query->result() as $follow) {
                
                $user = $this->db->where('id', $follow->userid)->get('tb_user')->row();
                $this->db->where('userid', $userid);
                $this->db->where('followid', $follow->userid);
                $query2 = $this->db->get('tb_follow');
                
                if ($query2->num_rows() > 0) {
                    $is_follow = 'Y';
                } else {
                    $is_follow = 'N';
                }
                
                $arr = array('userid' => $user->id,
                             'username' => $user->username,
                             'is_followed' => $is_follow,
                             'userimage' => $user->userphoto);
                array_push($result, $arr);
            }
            
            return $result;
        }
    }
    
    function user_profile($userid) {
        
        $result = array();
        $like_product = array();
        $list_product = array();
        
        if($this->db->where('id', $userid)->get('tb_user')->num_rows() == 0) return 0;
        
        $user = $this->db->where('id', $userid)->get('tb_user')->row();
        
        $followers = $this->db->where('followid', $user->id)->get('tb_follow')->num_rows();
        $followings = $this->db->where('userid', $user->id)->get('tb_follow')->num_rows();
        
        $this->db->where('userid', $userid);
        $like_query = $this->db->get('tb_like');
        if ($like_query->num_rows() > 0) {
            
            foreach($like_query->result() as $like) {
                
                $image = $this->db->where('product_id', $like->product_id)->get('tb_product_image')->row()->image;
                $product = $this->db->where('id', $like->product_id)->get('tb_product')->row();
                
                $arr = array('product_id' => $product->id,
                             'product_name' => $product->product_name,
                             'product_image' => $image);
                array_push($like_product, $arr);                
            }
        }
        
        $this->db->where('userid', $userid);
        $list_query = $this->db->get('tb_product');
        
        if ($list_query->num_rows() > 0) {
            
             foreach($list_query->result() as $list) {
                
                $image = $this->db->where('product_id', $list->id)->get('tb_product_image')->row()->image;                
                $arr = array('product_id' => $list->id,
                             'product_name' => $list->product_name,
                             'product_image' => $image);
                array_push($list_product, $arr);                
            }
        }
        
        
        $result['userid'] = $user->id;
        $result['username'] = $user->username;
        $result['userimage'] = $user->userphoto;
        $result['phone'] = $user->phone;
        $result['address'] = $user->address;
        $result['city'] = $user->city;
        $result['state'] = $user->state;
        $result['zip_code'] = $user->zip_code;
        $result['age'] = $user->age;
        $result['gender'] = $user->gender;
        $result['count_followers'] = $followers;
        $result['count_following'] = $followings;
        $result['is_verified'] = 1;  
        $result['verify_facebook'] = $user->verify_facebook;  
        $result['verify_google'] = $user->verify_google;  
        $result['list_of_products'] = $list_product;  
        $result['product_likes'] = $like_product;  
        $result['product_bought'] = array();
        
        return $result;                          
    }
    
    function update_user($userid, $username, $file_url) {
        
        $this->db->where('id', $userid);
        $this->db->set('username', $username);
        $this->db->set('userphoto', $file_url);
        $this->db->update('tb_user');
    }
    
    function follow_friend($userid, $friendid) {
        
        $this->db->where('userid', $userid);
        $this->db->where('followid', $friendid);
        
        if ($this->db->get('tb_follow')->num_rows() == 0) {
            
            $this->db->set('userid', $userid);
            $this->db->set('followid', $friendid);
            $this->db->insert('tb_follow');
        }                                  
    }
    
    function search_friend($userid, $searchtext) {
         
         $result = array();
         $this->db->where('id !=', $userid);
         $this->db->like('username', $searchtext);
         
         $query = $this->db->get('tb_user');
         
         if($query->num_rows() == 0) return 0;
        
        else {
            foreach($query->result() as $user) {
                
               $isFollow = 'N';
               $this->db->where('userid', $userid);
               $this->db->where('followid', $user->id);
               if ($this->db->get('tb_follow')->num_rows() > 0) {
                   
                   $isFollow = 'Y';
               } 
                
               $arr = array('userid' => $user->id,
                            'username' => $user->username,
                            'userimage' => $user->userphoto,
                            'phone' => $user->phone,
                            'is_followed' => $isFollow);
                array_push($result, $arr);
            }
            
            return $result;
        }
     }
     
    function my_friendlist($userid) {
         
         $reult = array();
         $this->db->where('id !=', $userid);
         
         $query = $this->db->get('tb_user');
         
         if($query->num_rows() == 0) return 0;
        
         else {
            foreach($query->result() as $user) {
                
                $arr = array('userid' => $user->id,
                             'username' => $user->username,
                             'phone' => $user->phone,
                             'userimage' => $user->userphoto);
                array_push($result, $arr);
            }
            
            return $result;
        }
     }
     
    function unfollow($userid, $friendid) {
         
         $this->db->where('userid', $userid);
         $this->db->where('followid', $friendid);
         $query = $this->db->get('tb_follow');
         
         if ($query->num_rows() > 0) {
             $this->db->where('userid', $userid);
             $this->db->where('followid', $friendid);
             $this->db->delete('tb_follow');
         }                                  
     }
     
    function setLike($userid, $productid) {
         
         $this->db->where('userid', $userid);
         $this->db->where('product_id', $productid);
         $query = $this->db->get('tb_like');
         
         if ($query->num_rows() == 0) {
             $this->db->set('userid', $userid);
             $this->db->set('product_id', $productid);
             $this->db->insert('tb_like');
             $this->db->insert_id();
         }
     }
     
    function setUnLike($userid, $productid) {
         
         $this->db->where('userid', $userid);
         $this->db->where('product_id', $productid);
         $query = $this->db->get('tb_like');
         
         if ($query->num_rows() > 0) {
             $this->db->where('userid', $userid);
             $this->db->where('product_id', $productid);
             $this->db->delete('tb_like');
         }
     }
     
    function setSold($productid, $type) {
         
         $this->db->where('id', $productid);
         $this->db->set('is_sold', $type);
         $this->db->update('tb_product');
     }
     
    function verify_facebook($userid) {
         
         $this->db->where('id', $userid);
         $this->db->set('verify_facebook', 'Y');
         $this->db->update('tb_user');          
     }
     
    function verify_google($userid) {
         
         $this->db->where('id', $userid);
         $this->db->set('verify_google', 'Y');
         $this->db->update('tb_user');          
     }
     
    
    function add_cart($userid, $productid, $count) {
        
        $this->db->where('userid', $userid);
        $this->db->where('productid', $productid);
        $query = $this->db->get('tb_cart');
         
        if ($query->num_rows() == 0) {
            $this->db->set('userid', $userid);
            $this->db->set('productid', $productid);
            $this->db->set('count', $count);
            $this->db->insert('tb_cart');
            $this->db->insert_id();
        }        
    }
    
    function remove_cart($userid, $productid) {        
        
        $this->db->where('userid', $userid);
        $this->db->where('productid', $productid);
        $query = $this->db->get('tb_cart');
         
        if ($query->num_rows() > 0) {
            $this->db->where('userid', $userid);
            $this->db->where('productid', $productid);
            $this->db->delete('tb_cart');
        }
    }
    
    function get_cart($user_id) {       
        
        $result = array();
        
        $query = $this->db->select('tb_product.*, tb_user.username')
                          ->from('tb_cart')
                          ->where('tb_cart.userid', $user_id)
                          ->join('tb_product', 'tb_product.id = tb_cart.productid')
                          ->join('tb_user', 'tb_user.id = tb_product.userid')  
                          ->get();
        
        
        if ($query->num_rows() == 0) return 0;
        
        else {    
            foreach($query->result() as $product) {
                
                $query_image = $this->db->where('product_id', $product->id)->get('tb_product_image');
                if ($query_image->num_rows() > 0) {
                    $image = $query_image->row()->image;
                } else {
                    $image = "N/A";
                }
                $arr = array('product_id' => $product->id,
                             'product_name' => $product->product_name,
                             'product_description' => $product->product_description,
                             'product_description' => $product->product_description,
                             'product_username' => $product->username,
                             'sold_count' => $product->sold_count,
                             'type' => $product->type,
                             'product_image' => $image);
                array_push($result, $arr);
            }
            
            return $result;        
        }
    
    }
    
    function filter_product($age, $gender, $categories) {
        
        $result = array();
        if ($age != 0) {
            $this->db->where('age_min <', $age);       
            $this->db->where('age_max >', $age);            
        }
        if ($gender != "") {
            if ($gender == "male") {
                $this->db->where('gender !=', 'female'); 
            } else {
                $this->db->where('gender !=', "male");
            }      
        }   
        
        $this->db->where('quantity_available > ', 0);
        $this->db->where('is_active', 'yes');
        $this->db->order_by('reg_date', 'DESC');
        
        $this->db->like('category', $categories[0]);
        if (count($categories) > 1) {
            for ($i = 0; $i < count($categories) - 2; $i++) {
                $this->db->or_like('category', $categories[$i+1]);
            }
        }
        $this->db->group_by('id');
        $query = $this->db->get('tb_product');
        
        if ($query->num_rows() == 0) return 0;
        
        else {    
            foreach($query->result() as $product) {
                
                $image = $this->db->where('product_id', $product->id)->get('tb_product_image')->row()->image;
                
                $arr = array('product_id' => $product->id,
                             'product_name' => $product->product_name,
                             'product_image' => $image);
                array_push($result, $arr);
            }
            
            return $result;        
        }
    }
    
    function exist_social_user($social_id) {
        
        $this->db->where('password', $social_id);
        $rows = $this->db->get('tb_user')->num_rows();
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    function social_signup($data) {
        
        $this->db->insert('tb_user', $data);
        return $this->db->insert_id();
        
    }
    
    function set_sold_product($productid, $count, $what) { 
        
        $this->db->where('id', $productid);
        if ($what == "add") {
            $this->db->set('sold_count', 'sold_count + '.$count, FALSE);
        } else {
            $this->db->set('sold_count', 'sold_count - '.$count, FALSE);
        }
        
        $this->db->update('tb_product');
        
    }
    
    function updateProduct($product_id, $p_name, $p_description, $p_price, $category, $lat, $lon,
         $quantity_available, $quantity_per_user, $age_min, $age_max, $gender, $hobby, $industry, $occupation, $zipcode, $city, $state, $country) {
             
         
        
        $this->db->where('id', $product_id);
        $this->db->set('product_name', $p_name);
        $this->db->set('product_description', $p_description);
        $this->db->set('product_price', $p_price);
        $this->db->set('category', $category);
        $this->db->set('latitude', $lat);
        $this->db->set('longitude', $lon);
        
        $this->db->set('quantity_available', $quantity_available);
        $this->db->set('quantity_per_user', $quantity_per_user);
        $this->db->set('age_min', $age_min);
        $this->db->set('age_max', $age_max);
        $this->db->set('gender', $gender);
        $this->db->set('hobby', $hobby);
        $this->db->set('industry', $industry);
        $this->db->set('occupation', $occupation);
        $this->db->set('zipcode', $zipcode);
        $this->db->set('state', $state);
        $this->db->set('city', $city);
        $this->db->set('country', $country);
        $this->db->set('reg_date', 'NOW()', false);

        $this->db->update('tb_product');            
         
    }
         
    function update_userinfo($id, $phone, $address, $city, $state, $zip_code, $age, $gender) {
        
        $this->db->where('id', $id);
        $this->db->set('phone', $phone);
        $this->db->set('address', $address);
        $this->db->set('city', $city);
        $this->db->set('state', $state);
        $this->db->set('zip_code', $zip_code);
        $this->db->set('age', $age);
        $this->db->set('gender', $gender);
        
        $this->db->update('tb_user');
        
    }
    
    function delete_product($product_id) {
        
        $this->db->where('id', $product_id);
        $this->db->delete('tb_product');
        
        $this->db->where('product_id', $product_id);
        $this->db->delete('tb_product_image');
    }
    
    function add_address($data) {
        
        $this->db->insert('tb_address', $data);
        return $this->db->insert_id();
    }
    
    function get_address($user_id) {
        
        $this->db->where('userid', $user_id);
        return $this->db->get('tb_address')->result_array();
    }
    
    function get_lastdate($user_id) {
        if ($this->db->where('userid', $user_id)->get('tb_order')->num_rows() > 0) {
        return $this->db->where('userid', $user_id)
                    ->order_by('id',"desc")
                    ->limit(1)
                    ->get('tb_order')
                    ->row_array();
        } else {
            return false;
        }
    }
    
    function add_order($data) {
        
        $this->db->insert('tb_order', $data);
        return $this->db->insert_id();
    }
    
    function get_seller($product_id) {
        
        return $this->db->select('t2.*')
                        ->from('tb_product as t1')
                        ->where('t1.id', $product_id)
                        ->join('tb_user as t2', 't1.userid = t2.id')
                        ->get()
                        ->row_array();
    }
    
    function add_transaction($t_data) {
        
        $this->db->insert('tb_transaction', $t_data);
        return $this->db->insert_id();
    }
    
    function update_balance($user_id, $amount) {
        
        $this->db->where('id', $user_id);
        $this->db->set('balance', 'balance - '.$amount, FALSE);
        $this->db->update('tb_user');
        
    }
    
    function inactive_product($product_id) {
        
        $this->db->where('id', $product_id);
        $this->db->set('is_active', 'no');
        $this->db->update('tb_product');
    }
    
    function add_balance($user_id, $amount) {
        
        $this->db->where('id', $user_id);
        $this->db->set('balance', 'balance + '.$amount, FALSE);
        $this->db->update('tb_user');         
    }
    
    function remove_balance($user_id, $amount) {
        
        $this->db->where('id', $user_id);
        $this->db->set('balance', 'balance - '.$amount, FALSE);
        $this->db->update('tb_user');         
    }
    
    function register_token($user_id, $token) {
        $this->db->where('id', $user_id);
        $this->db->set('token', $token);
        $this->db->update('tb_user');
    }
    
    function get_balance($user_id) {
        
        $this->db->where('id', $user_id);
        return $this->db->get('tb_user')->row()->balance;
    }
}
?>
