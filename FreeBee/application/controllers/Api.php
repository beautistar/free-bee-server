<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

         $this->doRespond(0, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    
    function sendPush($token = "", $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAA6IzU7gw:APA91bHwXn3b3DJjV9VNMmSzMjqZMir3Wf9GiujNsCqXQcnVXUrhxjlZDgfnX_qG6Y4ARdrAo1P9RfjJQQsXc1d3ub2zs0xZNT07DSdqeO-iT66gpAQjQ1H0vctJJWssaa2DZvf2ptLr";
        
        
        if (strlen($token) == 0 ) {

            return;
        }
        
        //       if(is_array($target)){
        //        $fields['registration_ids'] = $target;
        //       }else{
        //        $fields['to'] = $target;
        //           }

        // Tpoic parameter usage
        //        $fields = array
        //                    (
        //                        'to'  => '/topics/alerts',
        //                        'notification'          => $msg
        //                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        
        $msg = array
                (
                    'body'     => $body,
                    'title'    => APP_NAME,   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    }
    
    function charge_payment() {
        
        $result = array();            
        
        $stripe_token = $_POST['stripe_token'];
        $email = $_POST['email'];
        $amount = $_POST['amount'];

        $STRIPE_SECRET_KEY = "sk_test_D3dmeXiUEG95pXPACbByi9rI00ssgbxrLx";
        try {
            
            \Stripe\Stripe::setApiKey($STRIPE_SECRET_KEY);

            $message = \Stripe\Charge::create(array(
              "amount" => $amount,
              "currency" => "usd",
              "source" => $stripe_token, // obtained with Stripe.js
              "description" => "Paid from ".$email
            ));
            
            $result = $message;
            $this->doRespondSuccess($result);
            
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();//`enter code here`
            $err = $body['error'];
            $this->doRespond(204, $err);
            
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->doRespond(204, $err);
            //return $err['message'];
            
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
            
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
            
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
            
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
       
        }        
    }
    
    function refund_payment() {
        
        $result = array();   
        
        $transaction_id = $_POST['transaction_id'];
        $amount = $_POST['amount'];

        $STRIPE_SECRET_KEY = "sk_test_D3dmeXiUEG95pXPACbByi9rI00ssgbxrLx";
        try {
            
            \Stripe\Stripe::setApiKey($STRIPE_SECRET_KEY);
            //$ch = Stripe_Charge::retrieve('ch_19DReJIAgNbxQsQZsBTh85Ka');
            //$ch->refunds->create(array('amount' => 100));
            $refund= \Stripe\Refund::create(array(
              "amount" => $amount,
              "charge" => $transaction_id,
              'reason' => 'refund'
            ));
            
            $result = $refund;
            $balanceTransaction = \Stripe\BalanceTransaction::retrieve($refund->balance_transaction);
            $result['balanceTransaction'] = $balanceTransaction;
            $this->doRespondSuccess($result);
            
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();//`enter code here`
            $err = $body['error'];
            $this->doRespond(204, $err);
            
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->doRespond(204, $err);
            //return $err['message'];
            
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
            
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
            
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
            
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(204, $err);
       
        }        
    }
         
     ///////////////////////////////////////////
     ////////  20161124  ///////////////////////
     ////////   API start //////////////////////       
    
    
     
    function register() {
         
         $username = $this->doUrlDecode($_POST['username']);
         $email = $this->doUrlDecode($_POST['email']);
         $password = $this->doUrlDecode($_POST['password']);
         $phone = $_POST['phone'];
         $address = $_POST['address'];
         $city = $_POST['city'];
         $state = $_POST['state'];
         $zip_code = $_POST['zip_code'];
         $age = $_POST['age'];
         $gender = $_POST['gender'];
          
         $result = array();
         
         $exist = $this->api_model->existEmail($email);
         $exist_name = $this->api_model->existUserName($username);
         
         if ($exist > 0) {
             $result['error'] = "Email address is already exist.";
             $this->doRespond(203, $result);
             return;
         } else if ($exist_name > 0) {
             $result['error'] = "Username address is already exist.";
             $this->doRespond(206, $result);
             return;
         } else {
             
             $id = $this->api_model->register($username, $email, $password, $phone, $address, $city, $state, $zip_code, $age, $gender);
        
             if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
             }
             $upload_path = "uploadfiles/";  

             $cur_time = time();
             
             $dateY = date("Y", $cur_time);
             $dateM = date("m", $cur_time);
             
             if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
             }
             if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
             }
             
             $upload_path .= $dateY."/".$dateM."/";
             $upload_url = base_url().$upload_path;

             // Upload file. 

             $w_uploadConfig = array(
                'upload_path' => $upload_path,
                'upload_url' => $upload_url,
                'allowed_types' => "*",
                'overwrite' => TRUE,
                'max_size' => "100000KB",
                'max_width' => 4000,
                'max_height' => 4000,
                'file_name' => $dateY.$dateM.intval(microtime(true) * 10)
             );

             $this->load->library('upload', $w_uploadConfig);

             if ($this->upload->do_upload('photo')) {

                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                $data = array('userphoto' => $file_url);
                $this->api_model->update_photo($id, $data);                              

             } else {
                $result['error'] = "Photo upload faied.";
                $this->doRespond(203, $result);
                return;
             }
             
             
             $result['user_profile'] = $this->api_model->user_profile($id);
         
             if ($result['user_profile'] == 0) {
                 $result['user_profile'] = array();
                 $this->doRespond(1, $result);
                 return;
             }             
             $this->doRespondSuccess($result); 
         }        
    }
     
    function login() {
         
         $result = array();
         
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $exist = $this->api_model->existEmail($email);
         
         if ($exist == 0) {
             $result['error'] = "Email address does not exist.";
             $this->doRespond(204, $result);
             return;
         }
         
         $user = $this->db->where('email', $email)->get('tb_user')->row();
         if ($password != $user->password) {
             
             $result['error'] = "Wrong password.";
             $this->doRespond(205, $result);
             return;
             
         }
         $userid = $user->id;
         
         $result = array();
         
         $result['user_profile'] = $this->api_model->user_profile($userid);
         
         if ($result['user_profile'] == 0) {
             $result['user_profile'] = array();
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);          
    }
     
    function post_ad() {
         
         $result = array();
         
         $p_name = $_POST['product_name'];
         $p_description = $_POST['product_description'];
         $p_price = $_POST['product_price'];
         $category = $_POST['category'];
         $lat = $_POST['latitude'];
         $lon = $_POST['longitude'];
         $userid = $_POST['userid'];          
         $type = $_POST['type'];          
         
         $quantity_available = $_POST['quantity_available'];          
         $quantity_per_user = $_POST['quantity_per_user'];          
         $age_min = $_POST['age_min'];          
         $age_max = $_POST['age_max'];          
         $gender = $_POST['gender'];          
         $hobby = $_POST['hobby'];          
         $industry = $_POST['industry'];          
         $occupation = $_POST['occupation'];          
         $zipcode = $_POST['zipcode'];          
         $city = $_POST['city'];          
         $state = $_POST['state'];          
         $country = $_POST['country'];          
         
         
         
         $product_id = $this->api_model->saveProduct($p_name, $p_description, $p_price, $category, $lat, $lon, $userid, $type,
         $quantity_available, $quantity_per_user, $age_min, $age_max, $gender, $hobby, $industry, $occupation, $zipcode, $city, $state, $country);
         if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
         }
         
         if(!is_dir("uploadfiles/product/")) {
            mkdir("uploadfiles/product/");
         }
         $upload_path = "uploadfiles/product/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url()."uploadfiles/product/".$dateY."/".$dateM."/";

        // Upload file.

        for ($i = 0 ; $i < count($_FILES['product_image']['name']) ; $i++) {
         
            $filename = $_FILES['product_image']['name'][$i];
            
            $tmpFilePath = $_FILES['product_image']['tmp_name'][$i];
            $uploadFileName = $i.intval(microtime(true) * 10).'.png';

            move_uploaded_file($tmpFilePath, $upload_path.$uploadFileName);
            
            $file_url = $upload_url . $uploadFileName;            
             
            $this->api_model->saveProductFile($product_id, $file_url);
                        
        }
        
        $this->doRespondSuccess($result);
        return;
    }
     
    function product_list() {
         
         $result = array();
         $gender = "";
         $age = 0;
         if (isset($_POST["age"])) {
            $age = $_POST["age"];
         }
         if (isset($_POST["gender"])) {
            $gender = $_POST["gender"];
         }         
         if (isset($_POST["page"])) {
            $page = $_POST["page"];
         } else {
            $page = 1;
         }
        
         $result['product_list'] = $this->api_model->product_list($page, $age, $gender);
         
         if ($result['product_list'] == 0) {
             $result['product_list'] = array();
             $result['message'] = "empty";
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);
    }
     
    function product_detail() {
         
         $result = array();
         
         $product_id = $_POST['product_id'];
         $user_id = $_POST['userid'];
         
         $result['product_detail'] = $this->api_model->product_detail($user_id, $product_id);
         
         if ($result['product_detail'] == 0) {
             $result['product_detail'] = array();
             $result['message'] = "empty";
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);
    } 
     
    function search_product() {
         
         $result = array();
         $price_from = $_POST['price_from'];
         $price_to = $_POST['price_to'];
         $categories = $_POST['category'];
         
         $result['product_list'] = $this->api_model->search_product($price_from, $price_to, $categories);
         
         if ($result['product_list'] == 0) {
             $result['product_list'] = array();
             $result['message'] = "empty";
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);
    }
     
    function user_following() {
         
         $userid = $_POST['userid'];
         
         $result = array();
         
         $result['user_list'] = $this->api_model->userFollowing($userid);
         
         if ($result['user_list'] == 0) {
             $result['user_list'] = array();
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);
         
    }
     
    function user_follower() {
         
         $result = array();
         
         $userid = $_POST['userid'];
         
         $result = array();
         
         $result['user_list'] = $this->api_model->userFollower($userid);
         
         if ($result['user_list'] == 0) {
             $result['user_list'] = array();
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);
         
    }
     
    function user_profile() {
         
         $result = array();
         
         $userid = $_POST['userid'];
         
         $result = array();
         
         $result['user_profile'] = $this->api_model->user_profile($userid);
         
         if ($result['user_profile'] == 0) {
             $result['user_profile'] = array();
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);          
    }
     
    function update_user() {
         
         $result = array();
         
         $userid = $_POST['userid'];
         $username = $_POST['username'];
         
         $upload_path = "uploadfiles/user/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url()."uploadfiles/user/".$dateY."/".$dateM."/";

        // Upload file.

        $image_name = $userid."_file";

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $image_name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('user_photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $this->api_model->update_user($userid, $username, $file_url);
            
            //$result['userimage'] = $file_url;
            $this->doRespondSuccess($result);
            return;

        } else {

            $this->doRespond(202, $result);// upload fail
            return;
        }
    }
     
    function follow_friend() {
         
         $result = array();
         $userid = $_POST['userid'];
         $friendid = $_POST['friendid'];
         
         $this->api_model->follow_friend($userid, $friendid); 
         
         $this->doRespondSuccess($result);         
    }
     
    function search_friend() {
         
         $userid = $_POST['userid'];
         $searchtext = $_POST['searchtext'];
         
         $result = array();
         
         $result['user_list'] = $this->api_model->search_friend($userid, $searchtext);
         
         if ($result['user_list'] == 0) {
             $result['user_list'] = array();
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);         
    } 
     
    function my_friendlist() {
         
         $userid = $_POST['userid'];         
         
         $result = array();
         
         $result['user_list'] = $this->api_model->my_friendlist($userid);
         
         if ($result['user_list'] == 0) {
             $result['user_list'] = array();
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);         
    } 
     
    function unfollow() {
         
         $result = array();
         $userid = $_POST['userid'];
         $friendid = $_POST['friendid'];
         
         $this->api_model->unfollow($userid, $friendid);
         
         $this->doRespondSuccess($result);  
    }
     
    function like_product() {
         
         $result = array();
         $userid = $_POST['userid'];
         $productid = $_POST['productid'];
         $type = $_POST['type'];
         
         if ($type == 'L') {
             $this->api_model->setLike($userid, $productid);
         } else {
             $this->api_model->setUnLike($userid, $productid);
         }
         
         $this->doRespondSuccess($result);
    }
     
    function sold_product() {
         
         $result = array();
         $userid = $_POST['userid'];
         $productid = $_POST['productid'];
         $type = $_POST['type'];         
         
         $this->api_model->setSold($productid, $type);         
         
         $this->doRespondSuccess($result);          
    }
     
    function verify_facebook() {
         
         $result = array();
         $userid = $_POST['userid'];
         
         $this->api_model->verify_facebook($userid);         
         
         $this->doRespondSuccess($result);          
    }
     
    function verify_google() {
         
         $result = array();
         $userid = $_POST['userid'];
         
         $this->api_model->verify_google($userid);         
         
         $this->doRespondSuccess($result);          
    }  
    
    function add_cart_product() {
         
         $result = array();
         $userid = $_POST['userid'];
         $productid = $_POST['productid'];
         $count = $_POST['count'];
         
         $this->api_model->add_cart($userid, $productid, $count);
         
         $this->doRespondSuccess($result);
    }
    
    function get_cart_product() {
        
        $result = array();
        $userid = $_POST['userid'];         

        
        $result['product_list'] = $this->api_model->get_cart($userid);
         
        if ($result['product_list'] == 0) {
             $result['product_list'] = array();
             $result['message'] = "empty";
             $this->doRespond(1, $result);
             return;
        }
         
        $this->doRespondSuccess($result);          
        
    }
    
    function remove_cart() {
        
        $result = array();
        $userid = $_POST['userid'];
        $productid = $_POST['productid'];
        $count = $_POST['count'];
         
        $this->api_model->remove_cart($userid, $productid);
        
        $this->doRespondSuccess($result);
    }
    
    function filter_product() {
         
         $result = array();
         $gender = "";
         $age = 0;
         if (isset($_POST["age"])) {
            $age = $_POST["age"];
         }
         if (isset($_POST["gender"])) {
            $gender = $_POST["gender"];
         }
         $categories = $_POST['category'];
         $categories = explode(';', $categories);
         $result['product_list'] = $this->api_model->filter_product($age, $gender, $categories);
         
         if ($result['product_list'] == 0) {
             $result['product_list'] = array();
             $result['message'] = "empty";
             $this->doRespond(1, $result);
             return;
         }
         
         $this->doRespondSuccess($result);
    }
    
    function set_question() {
    }
    
    function social_signup() {
        
        $username = $this->doUrlDecode($_POST['username']);
        $email = $this->doUrlDecode($_POST['email']);
        $social_id = $this->doUrlDecode($_POST['social_id']);
        $photo_url =  $this->doUrlDecode($_POST['photo_url']);
        $social_type = $this->doUrlDecode($_POST['social_type']);
        
        if ($this->api_model->exist_social_user($social_id)) {
            
            $data = array('email' => $email, 'password' => $social_id);
            $user = $this->db->where($data)->get('tb_user')->row();

            $userid = $user->id;
             
            $result = array();
             
            $result['user_profile'] = $this->api_model->user_profile($userid);
             
            if ($result['user_profile'] == 0) {
                 $result['user_profile'] = array();
                 $this->doRespond(1, $result);
                 return;
            }
             
            $this->doRespondSuccess($result); 
            
            
        } else {
            
            $fb = 'N'; $gg = 'N';
            if ($social_type == 'facebook') {
                $fb = 'Y';
            } else {
                $gg = 'Y';
            }
            
            $data = array('username' => $username,
                          'email' => $email,
                          'password' => $social_id,
                          'userphoto' => $photo_url,
                          'verify_facebook' => $fb,
                          'verify_google' => $gg,
                          'reg_date' => date('Y-m-d h:m:s'));
            
            $this->api_model->social_signup($data);
            
            $userid = $this->api_model->social_signup($data);
            
            $result = array();
             
            $result['user_profile'] = $this->api_model->user_profile($userid);
             
            if ($result['user_profile'] == 0) {
                 $result['user_profile'] = array();
                 $this->doRespond(1, $result);
                 return;
            }
             
            $this->doRespondSuccess($result);              
        }
        
    }
    
    function social_login() {        

        $email = $this->doUrlDecode($_POST['email']);
        $social_id = $this->doUrlDecode($_POST['social_id']);
        
        if ($this->api_model->exist_social_user($social_id)) {
            $data = array('email' => $email, 'password' => $social_id);
            $user = $this->db->where($data)->get('tb_user')->row();

            $userid = $user->id;
             
            $result = array();
             
            $result['user_profile'] = $this->api_model->user_profile($userid);
             
            if ($result['user_profile'] == 0) {
                 $result['user_profile'] = array();
                 $this->doRespond(1, $result);
                 return;
            }
             
            $this->doRespondSuccess($result); 
                    
        } else {
            
            $result['error'] = "This social user does not exist.";
            $this->doRespond(204, $result);
            return;
        }
    }
    
    function register_userinfo() {
        
        $id = $_POST['id'];
        $phone = $_POST['phone'];
        $address = $_POST['address'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $zip_code = $_POST['zip_code'];
        $age = $_POST['age'];
        $gender = $_POST['gender'];
        
        $this->api_model->update_userinfo($id, $phone, $address, $city, $state, $zip_code, $age, $gender);
        $this->doRespondSuccess(array());  
         
    }
    
    // TODO: Edit product API 
    
    function edit_product() {
         
         $result = array();
         
         $product_id = $_POST['product_id'];
         $p_name = $_POST['product_name'];
         $p_description = $_POST['product_description'];
         $p_price = $_POST['product_price'];
         $category = $_POST['category'];
         $lat = $_POST['latitude'];
         $lon = $_POST['longitude'];
         
         $quantity_available = $_POST['quantity_available'];          
         $quantity_per_user = $_POST['quantity_per_user'];          
         $age_min = $_POST['age_min'];          
         $age_max = $_POST['age_max'];          
         $gender = $_POST['gender'];          
         $hobby = $_POST['hobby'];          
         $industry = $_POST['industry'];          
         $occupation = $_POST['occupation'];          
         $zipcode = $_POST['zipcode'];          
         $city = $_POST['city'];          
         $state = $_POST['state'];          
         $country = $_POST['country'];          
         
         $this->api_model->updateProduct($product_id, $p_name, $p_description, $p_price, $category, $lat, $lon,
         $quantity_available, $quantity_per_user, $age_min, $age_max, $gender, $hobby, $industry, $occupation, $zipcode, $city, $state, $country);
         
         
         if(isset($_FILES['product_image']) && $_FILES['product_image']['error'] != UPLOAD_ERR_NO_FILE) {
            
         
         /*if(file_exists($_FILES['product_image']['tmp_name'][0]) && isset($_FILES['product_image']))*/ 
         
             if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
             }
             
             if(!is_dir("uploadfiles/product/")) {
                mkdir("uploadfiles/product/");
             }
             $upload_path = "uploadfiles/product/";  

             $cur_time = time();
             
             $dateY = date("Y", $cur_time);
             $dateM = date("m", $cur_time);
             
             if(!is_dir($upload_path."/".$dateY)){
                 mkdir($upload_path."/".$dateY);
             }
             if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                 mkdir($upload_path."/".$dateY."/".$dateM);
             }
             
             $upload_path .= $dateY."/".$dateM."/";
             $upload_url = base_url()."uploadfiles/product/".$dateY."/".$dateM."/";

             // Upload file.

             for ($i = 0 ; $i < count($_FILES['product_image']['name']) ; $i++) {
             
                 $filename = $_FILES['product_image']['name'][$i];
                
                 $tmpFilePath = $_FILES['product_image']['tmp_name'][$i];
                 $uploadFileName = $i.intval(microtime(true) * 10).'.png';

                 move_uploaded_file($tmpFilePath, $upload_path.$uploadFileName);
                
                 $file_url = $upload_url . $uploadFileName;            
                 
                 $this->api_model->saveProductFile($product_id, $file_url);                             
            }             
         }
        
            $this->doRespondSuccess($result);
            return;
    }
    
    function delete_product() {
        
        $product_id = $_POST['product_id'];
        
        $this->api_model->delete_product($product_id);
        $this->doRespondSuccess(array());
    }
    
    function add_address() {
        
        $result = array();
        $name = $_POST['name'];
        $user_id = $_POST['userid'];
        $address = $_POST['address'];
        $state = $_POST['state'];
        $city = $_POST['city'];
        $zipcode = $_POST['zipcode'];
        
        $data = array('name' => $name,
                       'userid' => $user_id,
                       'address' => $address,
                       'state' => $state,
                       'city' => $city,
                       'zipcode' => $zipcode,
                       'created_at' => date('Y-m-d h:m:s'),
                       );
        
        $result['id'] = $this->api_model->add_address($data);
        $this->doRespondSuccess($result);
        
    }
    
    function get_address() {
        
        $result = array();
        $user_id = $_POST['userid'];
        $result['address_list'] = $this->api_model->get_address($user_id);
        $this->doRespondSuccess($result);
    }

    function check_out() {
        
        $result = array();
        $order_list = array();         

        $data = json_decode(file_get_contents('php://input'), true);

        $user_id =  $this->doUrlDecode($data['userid']);
        //$user_name =  $this->doUrlDecode($data['user_name']);
        //$email =  $this->doUrlDecode($data['email']);   
        $transaction_id = $this->doUrlDecode($data['transaction_id']);        
        $amount = $this->doUrlDecode($data['amount']);
        $address_id = $this->doUrlDecode($data['address_id']);

        $last_data = $this->api_model->get_lastdate($user_id);
        if ($last_data != false ) {
            
            $date1=date_create($last_data['ordered_at']);
            $date2=date_create(date('Y-m-d h:m:s'));
            $diff=date_diff($date1,$date2);
            $days = $diff->format("%a");
            if ($days < 30) {
                $remain = 30 - $days;
                $result['message'] = "You already have an order in this month. You can only make order once per month. You need to wait ".$remain." days";
                $this->doRespond(209, $result);
                return;
            }            
        } else {
        
            $order_list = $data["order_list"];
             
            foreach($order_list as $order) {
                
                $one = array('product_id' => $order['product_id'],
                             'count' => $order['count'],
                             //'type' => $order['type'],
                             'status' => 'ordered',
                             'userid' => $user_id,
                             'address_id' => $address_id,
                             'ordered_at' => date('Y-m-d h:m:s'));
                
                $this->api_model->add_order($one); 
                
                // update sold count in product table
                $this->api_model->set_sold_product($order['product_id'], $order['count'], "add");
                
                
                // send push notification 
                $item_type = $order['type'];                
                $seller = $this->api_model->get_seller($order['product_id']); 
                $content = "Your $item_type item has been sold.";                              
                $this->sendPush($seller['token'], "type", $content, "content");
                
                // update seller balance
                $amount = 0;
                if ($order['type'] == 'regular') {
                    $amount = 0.5 * $order['count'];
                } else {
                    $amount = 1.0 * $order['count'];
                }
                $this->api_model->update_balance($seller['id'], $amount); 
                // send warnning notification
                if ($seller['balance'] - $amount > 0 && $seller['balance'] - $amount < 5) {
                    $content = "Your balance is less than \$5, You need to charge.";
                    $this->sendPush($seller['token'], "type", $content, "content");
                } 
                
                if ($seller['balance'] - $amount < 0) {
                    $content = "Your balance is zero, Your items will not be activated until you charge.";
                    $this->sendPush($seller['token'], "type", $content, "content");
                    // set product inactive
                    
                    $this->api_model->inactive_product($order['product_id']);
                }                                       
            }
                                                     
            $t_data = array('userid' => $user_id,
                            'transaction_id' => $transaction_id,
                            'amount' => $amount,
                            'created_at' => date('Y-m-d h:m:s'));
            $this->api_model->add_transaction($t_data);
            $this->doRespondSuccess($result);
        }
    }
    
    function add_balance() {
        
        $user_id = $_POST['user_id'];
        $transaction_id = $_POST['transaction_id'];
        $amount = $_POST['amount'];
        
        $this->api_model->add_balance($user_id, $amount);
        
        $t_data = array('userid' => $user_id,
                            'transaction_id' => $transaction_id,
                            'amount' => $amount,
                            'created_at' => date('Y-m-d h:m:s'));
        $this->api_model->add_transaction($t_data);
        $this->doRespondSuccess(array());
        
    }
    
    function remove_balance() {
        
        $user_id = $_POST['user_id'];
        $transaction_id = $_POST['transaction_id'];
        $amount = $_POST['amount'];
        
        $this->api_model->remove_balance($user_id, $amount);
        
        $t_data = array('userid' => $user_id,
                         'transaction_id' => $transaction_id,
                         'amount' => (-1 * $amount),
                         'created_at' => date('Y-m-d h:m:s'));
        $this->api_model->add_transaction($t_data); 
        $this->doRespondSuccess(array());       
    }
    
    function register_token() {
        
        $user_id = $_POST['user_id'];
        $token = $_POST['token'];
        $this->api_model->register_token($user_id, $token);
        $this->doRespondSuccess(array());
    }
    
    function get_balance() {
        $result = array();
        $user_id = $_POST['user_id'];
        $result['balance'] = $this->api_model->get_balance($user_id);
        $this->doRespondSuccess($result);
        
    }
                  
}

?>
