<html>
    <head>
        <link href="https://assets.website-files.com/5c7d318eeaea1d6e1198d906/css/appcues-staging.dcaebdbb3.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.proof-x.com/proofx.js?ver=1581271916964" rel="preload" as="script">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CMuli:200,300,regular,600,700,800,900" media="all">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/appcues-public/website/css/main.css">
        <link rel="dns-prefetch" href="//app.omniconvert.com">

        <style type="text/css">  
        html.wf-loading * {opacity: 0;}
        .w-webflow-badge {display:none!important;}  
        input, textarea { -webkit-appearance: none; border-radius: 0; }
        h1, h2, h3, h4, h5, h6, p, a {-webkit-font-smoothing: antialiased;}
        .submit-trial-btn {font-weight: 600;letter-spacing: 0.3px;margin-bottom:20px;padding: 16px 38px;font-size: 16px;border-radius: 5.5px;transition: background 200ms ease;font-family: "Muli", sans-serif;cursor: pointer; line-height: 1.25; display:inline-block; text-align: center;}
        .submit-trial-btn.blurple-btn {color:#FFFFFF;background:#5C5CFF;border: 1px solid #3f3feb;}
        .submit-trial-btn.blurple-btn:hover {background: #3f3feb;}
        
        </style>
        <style type="text/css">
        .madkudu_modal {
        display: none;
        position: fixed;
        height: 100vh;
        width: 100vw;
        z-index: 1000;
        top: 0em;
        left: 0em;
        }
        .madkudu_modal.open {
        display: block;
        }
        .madkudu_modal__overlay {
        position: fixed;
        height: 100vh;
        width: 100vw;
        background: #4a90e2;
        opacity: 0.85;
        }
        .madkudu_modal__canvas {
        position: fixed;
        height: 100vh;
        width: 100vw;
        text-align: center;
        font-family: Open Sans, Helvetica Neue, Helvetica, sans-serif;
        z-index: 100001;
        }
        .madkudu_modal__box_parent {
        position: relative;
        width: 100%;
        height: 100%;
        }
        .madkudu_modal__box {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        }
        .madkudu_modal__title {
        font-size: 50px;
        line-height: 50px;
        color: #FFF;
        padding-bottom: 30px;
        text-transform: uppercase;
        letter-spacing: 2px;
        }
        .madkudu_modal__subtitle {
        color: #FFF;
        font-size: 24px;
        line-height: 35px;
        padding-bottom: 40px;
        }
        .madkudu_modal__button {
        line-height: 2em;
        color: #FFF;
        display: inline-block;
        text-transform: uppercase;
        letter-spacing: 1px;
        border: 0;
        cursor: pointer;
        text-decoration: none;
        outline: none;
        transition: background-color 0.2s ease-in, color 0.2s ease-in;
        }
        .madkudu_modal__button--primary {
        line-height: 2em;
        color: #FFF;
        display: inline-block;
        text-transform: uppercase;
        letter-spacing: 1px;
        border: 0;
        cursor: pointer;
        text-decoration: none;
        outline: none;
        transition: background-color 0.2s ease-in, color 0.2s ease-in;
        width: 350px;
        background-color: #077355;
        font-size: 19px;
        }
        .madkudu_modal__button--primary:hover {
        background-color: #066355;
        color: #FFF;
        }
        .madkudu_modal__button--secondary {
        line-height: 2em;
        color: #FFF;
        display: inline-block;
        text-transform: uppercase;
        letter-spacing: 1px;
        border: 0;
        cursor: pointer;
        text-decoration: none;
        outline: none;
        transition: background-color 0.2s ease-in, color 0.2s ease-in;
        width: 180px;
        margin-left: 20px;
        margin-right: 20px;
        text-decoration: underline;
        font-size: 15px;
        background-color: transparent;
        }
        .madkudu_modal__button--secondary:hover {
        color: #DDD;
        }
        .madkudu_modal__branding {
        color: #FFF;
        font-size: 13px;
        position: fixed;
        bottom: 20px;
        left: 20px;
        color: #d9d9d9;
        }
        .madkudu_modal__branding a {
        text-decoration: none;
        color: #FFF;
        letter-spacing: 1px;
        }
        </style>
    </head>
    <body data-gr-c-s-loaded="true" style="zoom: 1;">
        <header class="section grey legal"><div class="container center header"><div class="heading-wrap-30"><h1 class="heading-42">Privacy Policy<br></h1></div></div></header>
                    
		<section id="about" class="section">
            <div class="container center _900">                
                <div class="default-rich-text legal w-richtext">
					<h3>Overview</h3>
                    <p>Protecting your private information is our priority. This Statement of Privacy applies to <a>http://free-bee.co</a> and Free-Bee.co LLC. and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Free-Bee.co LLC include <a>http://free-bee.co</a> and <strong>Free-Bee</strong>. The <strong>Free-Bee</strong> website is a The purpose of <strong>Free-Bee</strong> is to facilitate the exchange of promotional products to targeted audiences. By using the <strong>Free-Bee</strong> website, you consent to the data practices described in this statement.</p>
                    <p><strong>Last updated:</strong> 02-09-2020</p>
                    <h3>Collection of your Personal Information</h3>
                    <p>In order to better provide you with products and services offered on our Site, <strong>Free-Bee</strong> may collect personally identifiable information, such as your:</p>
                    <ul>
                        <li>First and Last Name</li>
                        <li>Mailing Address</li>
                        <li>E-mail Address</li>
                        <li>Phone Number</li>
                        <li>Profile Pictures</li>
                        <li>Date of Birth</li>
                        <li>If you purchase <strong>Free-Bee</strong> products and services, we collect billing and credit card information. This information is used to complete the purchase transaction.</li>
                    </ul>
                    <p><strong>Free-Bee</strong> may also collect anonymous demographic information, which is not unique to you, such as your:</p>
                    <ul>
                        <li>Age</li>
                        <li>Gender</li>
                    </ul>
                    <p>We do not collect any personal information about you unless you voluntarily provide it to us. However, you may be required to provide certain personal information to us when you elect to use certain products or services available on the Site. These may include: (a) registering for an account on our Site; (b) entering a sweepstakes or contest sponsored by us or one of our partners; (c) signing up for special offers from selected third parties; (d) sending us an email message; (e) submitting your credit card or other payment information when ordering and purchasing products and services on our Site. To wit, we will use your information for, but not limited to, communicating with you in relation to services and/or products you have requested from us. We also may gather additional personal or non-personal information in the future.</p>
                    <h3>Use of your Personal Information</h3>
                    <p><strong>Free-Bee</strong> collects and uses your personal information to operate its website(s) and deliver the services you have requested.</p>
                    <p><strong>Free-Bee</strong> may also use your personally identifiable information to inform you of other products or services available from <strong>Free-Bee</strong> and its affiliates.</p>
                    <h3>Sharing Information with Third Parties</h3>
                    <p><strong>Free-Bee</strong> does not sell, rent or lease its customer lists to third parties.</p>
                    <p><strong>Free-Bee</strong> may share data with trusted partners to help perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to <strong>Free-Bee</strong>, and they are required to maintain the confidentiality of your information.</p>
                    <p><strong>Free-Bee</strong> may disclose your personal information, without notice, if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on <strong>Free-Bee</strong> or the site; (b) protect and defend the rights or property of <strong>Free-Bee</strong>; and/or (c) act under exigent circumstances to protect the personal safety of users of <strong>Free-Bee</strong>, or the public.</p>
                    <h3>Tracking User Behavior</h3>
                    <p><strong>Free-Bee</strong> may keep track of the websites and pages our users visit within <strong>Free-Bee</strong>, in order to determine what <strong>Free-Bee</strong> services are the most popular. This data is used to deliver customized content and advertising within <strong>Free-Bee</strong> to customers whose behavior indicates that they are interested in a particular subject area.</p>
                    <h3>Cursor movement tracking</h3>
                    <h3>Automatically Collected Information</h3>
                    <p>Information about your computer hardware and software may be automatically collected by <strong>Free-Bee</strong>. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the <strong>Free-Bee</strong> website.</p>
                    <h3>Use of Cookies</h3>
                    <p>The <strong>Free-Bee</strong> website may use "cookies" to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.</p>
                    <p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize <strong>Free-Bee</strong> pages, or register with <strong>Free-Bee</strong> site or services, a cookie helps <strong>Free-Bee</strong> to recall your specific information on subsequent visits. This simplifies the process of recording your personal information, such as billing addresses, shipping addresses, and so on. When you return to the same <strong>Free-Bee</strong> website, the information you previously provided can be retrieved, so you can easily use the <strong>Free-Bee</strong> features that you customized.</p>
                    <p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the <strong>Free-Bee</strong> services or websites you visit.</p>
                    <h3>Security of your Personal Information</h3>
                    <p><strong>Free-Bee</strong> secures your personal information from unauthorized access, use, or disclosure. <strong>Free-Bee</strong> uses the following methods for this purpose:</p>
                    <h4>SSL Protocol</h4>
                    <p>When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol.</p>
                    <p>We strive to take appropriate security measures to protect against unauthorized access to or alteration of your personal information. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, you acknowledge that: (a) there are security and privacy limitations inherent to the Internet which are beyond our control; and (b) security, integrity, and privacy of any and all information and data exchanged between you and us through this Site cannot be guaranteed.</p>
                    <h4>Children Under Thirteen</h4>
                    <p><strong>Free-Bee</strong> does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this website.</p>
                    <p>Disconnecting your <strong>Free-Bee</strong> Account from Third Party Websites
                        You will be able to connect your <strong>Free-Bee</strong> account to third party accounts. BY CONNECTING YOUR MUTINYRE ACCOUNT TO YOUR THIRD PARTY ACCOUNT, YOU ACKNOWLEDGE AND AGREE THAT YOU ARE CONSENTING TO THE CONTINUOUS RELEASE OF INFORMATION ABOUT YOU TO OTHERS (IN ACCORDANCE WITH YOUR PRIVACY SETTINGS ON THOSE THIRD PARTY SITES). IF YOU DO NOT WANT INFORMATION ABOUT YOU, INCLUDING PERSONALLY IDENTIFYING INFORMATION, TO BE SHARED IN THIS MANNER, DO NOT USE THIS FEATURE. You may disconnect your account from a third party account at any time. Users may disconnect Facebook and Google accounts by visiting facebook.com and google.com</p>
                    <h4>E-mail Communications</h4>
                    <p>From time to time, <strong>Free-Bee</strong> may contact you via email for the purpose of providing announcements, promotional offers, alerts, confirmations, surveys, and/or other general communication. In order to improve our Services, we may receive a notification when you open an email from <strong>Free-Bee</strong> or click on a link therein.</p>
                    <p>If you would like to stop receiving marketing or promotional communications via email from <strong>Free-Bee</strong>, you may opt out of such communications by changing preferences in their user portal.</p>
                    <h3>Changes to this Statement</h3>
                    <p><strong>Free-Bee</strong> reserves the right to change this Privacy Policy from time to time. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your account, by placing a prominent notice on our site, and/or by updating any privacy information on this page. Your continued use of the Site and/or Services available through this Site after such modifications will constitute your: (a) acknowledgment of the modified Privacy Policy; and (b) agreement to abide and be bound by that Policy.</p>
                    <h3>Contact Information</h3>
                    <p><strong>Free-Bee</strong> welcomes your questions or comments regarding this Statement of Privacy. If you believe that <strong>Free-Bee</strong> has not adhered to this Statement, please contact us at:</p>
                    <p>Free-Bee.co LLC<br>11500 sw 131 ave<br>Miami, Florida 33186</p>
                    <p><strong>Email Address:</strong> support@free-bee.co</p>
                    <p><strong>Telephone number:</strong> 7865129368</p>
                </div>
            </div>
        </section>
    </body>
</html>